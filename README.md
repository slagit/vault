# SLAGIT.net Email Server

## Installation

1. Create a new Linode

   Follow instructions
   [here](https://gitlab.com/slagit/nixos-install-linode) to provision
   a new linode.

   ```sh
   sudo nixos-install --flake gitlab:slagit/vault#morgan
   sudo nixos-enter -- passwd albert
   ```

   Ensure DNS records (below) are set before the final reboot.

1. Add A/AAAA, CNAME, and reverse DNS records

   ```
   slagit-vault.net. 300 IN A 203.0.113.34
   slagit-vault.net. 300 IN AAAA 2001:db8::34
   morgan.slagit-vault.net. 300 IN A 203.0.113.34
   morgan.slagit-vault.net. 300 IN AAAA 2001:db8::34
   ```

1. Initialize and unseal the vault server

   ```sh
   export VAULT_ADDR=https://slagit-vault.net:8200
   nix develop
   vault operator init -key-shares=1 -key-threshold=1
   vault operator unseal
   ```

1. Provision vault resources

   ```sh
   export TF_HTTP_ADDRESS="https://gitlab.com/api/v4/projects/45668918/terraform/state/morgan";
   export TF_HTTP_LOCK_ADDRESS="https://gitlab.com/api/v4/projects/45668918/terraform/state/morgan/lock";
   export TF_HTTP_LOCK_METHOD="POST";
   export TF_HTTP_PASSWORD=glpat-************
   export TF_HTTP_RETRY_WAIT_MIN=5;
   export TF_HTTP_UNLOCK_ADDRESS="https://gitlab.com/api/v4/projects/45668918/terraform/state/morgan/lock";
   export TF_HTTP_UNLOCK_METHOD="DELETE";
   export TF_HTTP_USERNAME=kocha
   export VAULT_ADDR=https://slagit-vault.net:8200
   export VAULT_TOKEN=hvs.************
   nix build .#provisioning-http --out-link vault.tf.json
   terraform plan -out tfplan
   terraform apply
```

1. Revoke root token

   ```sh
   vault token revoke -self
   ```

## Create a new userpass entry

   Note: An identity for the user should be added using the terraform
   provisioning configuration first.

   ```sh
   vault write auth/userpass/users/<username> password=<password> token_ttl=1h token_max_ttl=8h
   ```
