{ lib, ... }: let
  role_defaults = {
    backend = "\${vault_mount.ssh_client_signer.path}";
    key_type = "ca";
    allow_user_certificates = true;
    default_extensions = {
      permit-pty = "";
    };
    allowed_extensions = "permit-pty";
    max_ttl = "300";
  };
  role_policy = name: roles: {
    inherit name;
    policy = ''
      path "ssh-client-signer/roles" {
        capabilities = ["list"]
      }
    '' + lib.concatStrings (map (r: ''
      path "ssh-client-signer/sign/${r}" {
        capabilities = ["update"]
        denied_parameters = {
          "key_id" = []
        }
      }
    '') roles);
  };
in {
  resource = {
    vault_mount.ssh_client_signer = {
      type = "ssh";
      path = "ssh-client-signer";
    };
    vault_ssh_secret_backend_ca.client_signer = {
      backend = "\${vault_mount.ssh_client_signer.path}";
      generate_signing_key = true;
    };
    vault_ssh_secret_backend_role = {
      client_user = role_defaults // {
        name = "user";
        allowed_users = "{{identity.entity.metadata.ssh_username}}";
        allowed_users_template = true;
      };
      client_nixos = role_defaults // {
        name = "nixos";
        allow_user_certificates = true;
        allowed_users = "nixos";
        default_user = "nixos";
      };
    };
    vault_policy = {
      ssh_client_nixos = role_policy "ssh-nixos" [ "nixos" ];
      ssh_client_user = role_policy "ssh-user" [ "user" ];
    };
  };
  slagit.vault.groups = {
    server-installers = {
      policies = ["ssh-nixos"];
    };
    server-users = {
      policies = ["ssh-user"];
    };
  };
}
