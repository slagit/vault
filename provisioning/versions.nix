{ config, lib, ... }: {
  config = {
    terraform = {
      backend = config.slagit.vault.backend;
      required_providers.vault = {
        source = "hashicorp/vault";
        version = "3.11.0";
      };
      required_version = "1.3.9";
    };
  };
  options.slagit.vault.backend = lib.mkOption {
    default = {};
    type = lib.types.attrsOf lib.types.anything;
  };
}
