{ ... }: {
  resource.vault_policy.admin = {
    name = "admin";
    policy = ''
      path "auth/token/create" {
        capabilities = ["update"]
      }
      path "auth/userpass/users" {
        capabilities = ["list"]
      }
      path "auth/userpass/users/*" {
        capabilities = ["create", "read", "update"]
      }
      path "identity/group" {
        capabilities = ["update"]
      }
      path "identity/group/id" {
        capabilities = ["list"]
      }
      path "identity/group/id/*" {
        capabilities = ["delete", "read", "update"]
      }
      path "identity/entity" {
        capabilities = ["update"]
      }
      path "identity/entity/merge" {
        capabilities = ["update"]
      }
      path "identity/entity/id" {
        capabilities = ["list"]
      }
      path "identity/entity/id/*" {
        capabilities = ["delete", "read", "update"]
      }
      path "identity/entity-alias" {
        capabilities = ["update"]
      }
      path "identity/entity-alias/id/*" {
        capabilities = ["delete", "read"]
      }
      path "identity/lookup/entity" {
        capabilities = ["update"]
      }
      path "ssh-client-signer/config/ca" {
        capabilities = ["delete", "read", "update"]
      }
      path "ssh-client-signer/roles/*" {
        capabilities = ["read", "update"]
      }
      path "sys/audit" {
        capabilities = ["read", "sudo"]
      }
      path "sys/auth" {
        capabilities = ["read"]
      }
      path "sys/mounts" {
        capabilities = ["read"]
      }
      path "sys/policies/acl" {
        capabilities = ["list"]
      }
      path "sys/policies/acl/*" {
        capabilities = ["read", "update"]
      }
    '';
  };
}
