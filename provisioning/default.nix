{ ... }: {
  imports = [
    ./audit.nix
    ./admin.nix
    ./ssh.nix
    ./users.nix
    ./versions.nix
  ];
}
