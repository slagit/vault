{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    nixos-common.url = "gitlab:slagit/nixos-common";
    open-acme = {
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
      url = "gitlab:slagit/open-acme";
    };
    terranix = {
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
      url = "github:terranix/terranix";
    };
  };
  outputs = { self, nixos-common, nixpkgs, open-acme, terranix, ... }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
    };
  in {
    packages."${system}" = {
      provisioning = terranix.lib.terranixConfiguration {
        inherit system;
        modules = [
          ./provisioning
          ./provisioning/local.nix
        ];
      };
      provisioning-http = terranix.lib.terranixConfiguration {
        inherit system;
        modules = [
          ./provisioning
          ./provisioning/http.nix
        ];
      };
      sv = pkgs.buildGoModule {
        name = "sv";
        src = ./sv;
        vendorHash = "sha256-7gcpw41mSxdNFqBwp+VRICPlNbQHUrc8vc8SI83yCpU=";
      };
      nixosConfigurations = {
        morgan = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            ./hosts/morgan.nix
          ];
          specialArgs = {
            inherit nixos-common;
          };
        };
        morgan-vbox = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            ./hosts/morgan-vbox.nix
          ];
          specialArgs = {
            inherit nixos-common open-acme;
          };
        };
      };
    };
    devShells."${system}".default = pkgs.mkShell {
      packages = [
        pkgs.terraform
        pkgs.vault
      ];
    };
  };
}
