{ nixos-common, ... }: {
  imports = [
    nixos-common.nixosModules.hardware.linode
    nixos-common.nixosModules.common
    nixos-common.nixosModules.users
    nixos-common.nixosModules.zerotier
    ../modules/vault.nix
  ];
  networking = {
    domain = "slagit-vault.net";
    hostName = "morgan";
  };
  slagit.vault.hostName = "slagit-vault.net";
}
