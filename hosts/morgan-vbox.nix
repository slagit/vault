{ nixos-common, open-acme, ... }: {
  imports = [
    nixos-common.nixosModules.hardware.virtualbox
    nixos-common.nixosModules.common
    nixos-common.nixosModules.users
    nixos-common.nixosModules.zerotier
    open-acme.nixosModules.open-acme
    ../modules/vault.nix
  ];
  networking = {
    domain = "dev-slagit-vault.net";
    hostName = "morgan-vbox";
  };
  slagit.vault.hostName = "dev-slagit-vault.net";
}
