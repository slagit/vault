package main

import (
	"crypto/ed25519"
	"crypto/rand"
	"encoding/pem"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"path"

	"github.com/hashicorp/vault/api"
	"github.com/mikesmitty/edkey"
	"golang.org/x/crypto/ssh"
)

var (
	mount_path = flag.String("mount-path", "ssh-client-signer", "vault path for ssh certficate authority")
	principal = flag.String("principal", getUserName(), "ssh principal for credential")
	role = flag.String("role", "user", "vault role for ssh credential")
)

func getUserName() string {
	u, err := user.Current()
	if err != nil {
		panic(err)
	}
	return u.Username
}

func writePrivateKey(filename string, priv ed25519.PrivateKey) error {
	privBytes := edkey.MarshalED25519PrivateKey(priv)

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}

	if err := pem.Encode(f, &pem.Block{
		Bytes: privBytes,
		Type: "OPENSSH PRIVATE KEY",
	}); err != nil {
		f.Close()
		return err
	}

	return f.Close()
}

func signPublicKey(pub ed25519.PublicKey) (string, error) {
	sshPub, err := ssh.NewPublicKey(pub)
	if err !=  nil {
		return "", err
	}

	cfg := api.DefaultConfig()
	if err := cfg.ReadEnvironment(); err != nil {
		return "", err
	}
	client, err := api.NewClient(cfg)
	if err != nil {
		return "", err
	}

	s, err := client.Logical().Write(fmt.Sprintf("%s/sign/%s", *mount_path, *role), map[string]interface{}{
		"public_key": string(ssh.MarshalAuthorizedKey(sshPub)),
		"valid_principals": principal,
	})
	if err != nil {
		return "", err
	}

	return s.Data["signed_key"].(string), nil
}

func main() {
	flag.Parse()
	args := flag.Args()

	tempDir, err := os.MkdirTemp("", "sv-");
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(tempDir)
	certPath := path.Join(tempDir, "id_ed25519-cert.pub")
	privPath := path.Join(tempDir, "id_ed25519")

	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		panic(err)
	}

	cert, err := signPublicKey(pub)
	if err != nil {
		panic(err)
	}

	if err := writePrivateKey(privPath, priv); err != nil {
		panic(err)
	}

	if err := os.WriteFile(certPath, []byte(cert), 0600); err != nil {
		panic(err)
	}

	sshArgs := []string{"-i", privPath}
	for _, a := range args {
		sshArgs = append(sshArgs, a)
	}

	cmd := exec.Command("ssh", sshArgs...)
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		panic(err)
	}
}
